CREATE DATABASE DATA_ENGINEER;

USE DATA_ENGINEER;

CREATE TABLE playlist (
    playlist_name VARCHAR(100) NOT NULL,
    song_name VARCHAR(100) NOT NULL,
    artist VARCHAR(100) NOT NULL,
    album VARCHAR(100),
    CONSTRAINT PK_Person PRIMARY KEY (playlist_name, song_name, artist)
);

INSERT INTO DATA_ENGINEER.playlist (playlist_name, song_name, artist) VALUES ('#1', 'Confia no Pai', 'Matheus Dos S Silva');
INSERT INTO DATA_ENGINEER.playlist (playlist_name, song_name, artist) VALUES ('#1', 'Me respeita nessa Porr@', 'Jeere Dos S Silva');
INSERT INTO DATA_ENGINEER.playlist (playlist_name, song_name, artist) VALUES ('#1', 'Palhaco', 'Raquel Dos S Silva');
INSERT INTO DATA_ENGINEER.playlist (playlist_name, song_name, artist) VALUES ('#1', 'Voce nao perdeu nada aqui', 'Phelipe S Silva');
INSERT INTO DATA_ENGINEER.playlist (playlist_name, song_name, artist) VALUES ('#1', 'Ah cala a boca', 'Julia Gouvea');

SELECT * FROM DATA_ENGINEER.playlist
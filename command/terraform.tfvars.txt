# podemos definir as variaveis assim tambem
# Caso tenhamos muitos arquivos setamos assim
# terraform -var-file="tfvars/qa.tfvars"

project_name = "data_platform"
enviroment      = "prod"
bucket_names    = ["raw", "processed", "curated"]
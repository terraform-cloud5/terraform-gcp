module "buckets" {
    source = "./datalake"
    region = "us-east1"
    zone = "us-east1-a"
    location = "us-east1"
    environment = "production"
    prefix = "stack"
    project = var.project
    project_id = "gcp-data-pipeline-stack"
    stotage_class_standard = "STANDARD"
    stotage_class_nearline = "NEARLINE"
    stotage_class_coldline = "COLDLINE"
    stotage_class_archive = "ARCHIVE"
    bucket_names = ["raw", "processing", "curated"]
    members = ["serviceAccount:sa-terraform-stack@gcp-data-pipeline-stack.iam.gserviceaccount.com",
    "user:mattheusxs@gmail.com"]
}
# variable "name_vm" {
#     description = ""
# }

# variable "machine_type" {
#     description = ""
# }

# variable "region" {
#     description = ""
# }

# variable "zone" {
#     description = ""
# }

variable "location" {
    default = "us-east1"
    description = ""
}

variable "environment" {
    default     = "development"
    description = ""
}

variable "prefix" {
    default     = "data-plataform"
    description = ""
}

variable "project" {
    default     = "gcp-data-pipeline-stack"
    description = ""
}

variable "project_id" {
    default     = "gcp-data-pipeline-stack"
    description = ""
}
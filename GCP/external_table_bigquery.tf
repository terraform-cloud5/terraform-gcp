# resource "google_bigquery_dataset" "dataset_stack" {
#     dataset_id      = "data_plataform_stack"
#     friendly_name   = "data_plataform_stack"
#     description     = "Data Plataform - stack data processing SFTP"
#     location        = var.location
#     project         = var.project
# }

# ############# TABLE Stack - Table Externa #############
# resource "google_bigquery_table" "table_stack_01" {
#     dataset_id      = google_bigquery_dataset.dataset_stack.dataset_id
#     table_id        = "data_plataform_stack_trip"
#     description     = "Tabela com dados processado SFTP stack"
#     project         = var.project

#     external_data_configuration {
#         autodetect      = true
#         source_format   = "PARQUET"

#         source_uris = [
#             "gs://bkt-stack-curated-layer-prd/fhvhv_tripdata_2022-04.parquet"
#         ]
#     }
# }


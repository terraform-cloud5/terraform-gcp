variable "region" {
    description = ""
}

variable "zone" {
    description = ""
}

variable "location" {
    description = ""
}

variable "environment" {
    description = ""
}

variable "prefix" {
    description = ""
}

variable "project" {
    description = ""
}

variable "project_id" {
    description = ""
}

variable "stotage_class_standard" {
    description = ""
}

variable "stotage_class_nearline" {
    description = ""
}

variable "stotage_class_coldline" {
    description = ""
}

variable "stotage_class_archive" {
    description = ""
}

variable "bucket_names" {
    description = ""
}

variable "members" {
    description = ""
}



